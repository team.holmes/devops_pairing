
Hello!

The purpose of this excercise is to understand how you go about performing the various tasks below.

You may not complete them all given the timeframe.

What is important is your reasoning and approach.

Please tackle each step in turn - take your time and ask as many questions as you need or that will set you up for success.

## Step 1
Pull down the html content of the following web page and save a file called index.html (do not use a web browser to obtain the html)
http://www.columbia.edu/~fdc/sample.html

## Step 2
Create a certificate using the following code
```
openssl genrsa -aes256 -passout pass:gsahdg -out server.pass.key 4096
openssl rsa -passin pass:gsahdg -in server.pass.key -out output1.out
openssl req -new -key output1.out -out server.csr
openssl x509 -req -sha256 -days 365 -in server.csr -signkey output1.out -out output2.out
rm -rf server.scr server.pass.key server.csr
```

## Step 3
Create a dockerfile that serves up the index.html you content previously downloaded.

Also copy the public key generated above to the same location.

We suggest you use https://hub.docker.com/_/nginx. (Feel free to read the details the page to understand more about file paths etc.)

You should only be concerned with building the docker image at this stage.

## Step 4
Create a bash script that will do the following.

Ask the user for their surname

Echo out the surname

Build the previously created docker image and tag it in the format [surname]:1.0.0

Note that docker tags should all be lowercase so you might want to consider adding this logic into the script.

## Step 5
Edit the following code below and deploy into the local kubernetes cluster
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: example
  namespace: default
  labels:
    app: html
    version: 1.0.0
spec:
  replicas: 1
  selector:
    matchLabels:
      app: html
      version: 1.0.0
  template:
    metadata:
      labels:
        app: html
        version: 1.0.0
    spec:
      containers:
      - name: express
        image: image
        imagePullPolicy: Never
        readinessProbe:
          initialDelaySeconds: 1
          periodSeconds: 2
          timeoutSeconds: 1
          successThreshold: 1
          failureThreshold: 1
          httpGet:
            host:
            scheme: HTTP
            path: /health
            httpHeaders:
            - name: Host
              value: localhost.com
            port: 80
          initialDelaySeconds: 10
          periodSeconds: 5
          successThreshold: 1
          failureThreshold: 5
        ports:
        - containerPort: 80
```

## Step 6
Using the command line, check to see if the pod is up and running, if not, please debug (solve any problems through code deleting rather than fixing any mis-configuration).

When your container is up and running, use local port-forwarding and view the served up content in a browser.

If a web page is not displayed then please start to debug.

Thank you for allowing us the opportunity to work with you.



